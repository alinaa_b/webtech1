
const FIRST_NAME = "Alina";
const LAST_NAME = "Bica";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value){
   if( value==NaN || value==Infinity || value == (-Infinity) || value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER){
    return NaN
   }else{
    return parseInt(value);
   }
}
console.log(numberParser(56));
console.log(numberParser('56.3'));
console.log(numberParser(NaN));
console.log(numberParser(Infinity));
console.log(numberParser(-Infinity));
console.log(numberParser(Number.MIN_VALUE));
console.log(numberParser(Number.MAX_VALUE));

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

